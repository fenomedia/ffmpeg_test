<?php
require 'vendor/autoload.php';

$videoDatei = '__PFAD_ZUR_DATEI__';

$ffmpeg = FFMpeg\FFMpeg::create([
    'ffmpeg.binaries'  =>  __DIR__ . '/bin/ffmpeg',
    'ffprobe.binaries' =>  __DIR__ . '/bin/ffprobe'
]);
$video = $ffmpeg->open($videoDatei);
$video
    ->filters()
    ->resize(new FFMpeg\Coordinate\Dimension(320, 240))
    ->synchronize();
$video
    ->frame(FFMpeg\Coordinate\TimeCode::fromSeconds(10))
    ->save('frame.jpg');
$video
    ->save(new FFMpeg\Format\Video\X264('aac', 'libx264'), 'export-x264.mp4');