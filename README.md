**Eine kurze Anleitung für PHP-FFmpeg**

Mit folgenden Schritten habe ich es zum laufen gebracht. Btw. ich führe PHP auf der Kommandozeile aus, um Timeouts zu vermeiden.
Also

```bash
php index.php
```

---

## Composer Installation

Für die Installation wird Composer benötigt.

1. Geh auf [Composer Installation für Windows](https://getcomposer.org/doc/00-intro.md#installation-windows) und folge den Anweisungen.
2. Danach kannst du im Projektverzeichnis einfach composer install ausführen.
3. Lies mal die [Composer Basics](https://getcomposer.org/doc/01-basic-usage.md), wenn du hängen bleiben solltest.

---

## Installation von FFmpeg-Binaries

Als Basis nutzt die PHP-Library installierte FFmpeg-Binaries. Die musst du erst installieren:

1. Geh die Schritte durch [Anleitung](https://de.wikihow.com/FFmpeg-unter-Windows-installieren)
2. Wichtig ist, dass ffmpeg in deiner systemweiten PATH-Variable drin ist
3. PHP-FFmpeg schaut in der PATH Var nach und führ darüber ffmpeg als CLI aus.

## Installation von Git als Versionierungs-Tool

Lade dir Git einfach [hier](https://git-scm.com/download/win) runter und installiere es.

---

## Klone das Repository und probiere es aus ;)

Ich habe meinen Code, welcher bei mir zum Konvertieren funktionierte, mal hier eingecheckt.

1. Klone meinen Code mit ```git clone https://fenomedia@bitbucket.org/fenomedia/ffmpeg_test.git```
2. Gehe in das entstandene Verzeichnis
3. Führe ```composer install```aus, um die Abhängigkeiten zu installieren
4. Lege ein Video in den Pfad (vorzugsweise ein mp4)
5. Passe den Pfad an
6. Führe PHP aus mit ```php index.php```

Das sollte es gewesen sein. Happy coding ;-)